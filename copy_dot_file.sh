#!/bin/bash

file_list="dot_vimrc dot_gitconfig dot_bashrc dot_irbrc dot_ansible.cfg dot_bash_aliases dot_railsrc dot_vimrc dot_rvmrc dot_screenrc"
for file in $file_list
do
    echo "Copy and rename ${file} in dotfiles to ${file//dot_/.}"
    cp $file ~/${file//dot_/.}
done

